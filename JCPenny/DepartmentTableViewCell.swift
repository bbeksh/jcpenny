//
//  DepartmentTableViewCell.swift
//  JCPenny
//
//  Created by Bibek Shrestha on 2/26/19.
//  Copyright © 2019 Bibek Shrestha. All rights reserved.
//

import UIKit

class DepartmentTableViewCell: UITableViewCell {

    @IBOutlet weak var DepartmentTitle: UILabel!
    
    @IBOutlet weak var DepartmentImageview: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
