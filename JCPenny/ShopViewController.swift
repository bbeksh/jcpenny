//
//  ShopViewController.swift
//  JCPenny
//
//  Created by Bibek Shrestha on 2/26/19.
//  Copyright © 2019 Bibek Shrestha. All rights reserved.
//

import UIKit

class ShopViewController: UIViewController , UITableViewDataSource, UITableViewDelegate{
    let departmentTitles:[String] = ["For The Home","Bed & Bath","Window","Appliances","Women"]
    let departmentImages: [UIImage] = [UIImage(named: "offer1")!,UIImage(named: "offer2")!,UIImage(named: "offer3")!,UIImage(named: "offer4")!,UIImage(named: "offer5")!]
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return departmentTitles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let departmentCell = tableView.dequeueReusableCell(withIdentifier: "DepartmentCell", for: indexPath) as! DepartmentTableViewCell
        
        departmentCell.DepartmentTitle.text = departmentTitles[indexPath.item]
        departmentCell.DepartmentImageview.image = departmentImages[indexPath.item]
        
        return departmentCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
