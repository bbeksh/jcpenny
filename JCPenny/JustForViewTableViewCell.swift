//
//  JustForViewTableViewCell.swift
//  JCPenny
//
//  Created by Bibek Shrestha on 2/26/19.
//  Copyright © 2019 Bibek Shrestha. All rights reserved.
//

import UIKit

class JustForViewTableViewCell: UITableViewCell {

    @IBOutlet weak var JFYImageview: UIImageView!
    
    @IBOutlet weak var JFYTitle: UILabel!
    
//    @IBOutlet weak var JFYSubtitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
