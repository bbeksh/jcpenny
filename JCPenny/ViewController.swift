//
//  ViewController.swift
//  JCPenny
//
//  Created by Bibek Shrestha on 2/16/19.
//  Copyright © 2019 Bibek Shrestha. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var adsColView: UICollectionView!
    @IBOutlet weak var promColView: UICollectionView!
    @IBOutlet weak var recentlyViewedColView: UICollectionView!
    
    let images: [UIImage] = [
        UIImage(named: "offer1")!,UIImage(named: "offer2")!,UIImage(named: "offer3")!,UIImage(named: "offer4")!,UIImage(named: "offer5")!]
   //check
    let promoArray: [String] = ["Extra 10% off", "Extra 10% off","Extra 10% off", "Extra 10% off","Extra 10% off"]
    
    let recentViewedImages: [UIImage] = [
        /*UIImage(named: "recent1")!,*/UIImage(named: "recent2")!,UIImage(named: "recent3")!]
    
    let JFYImages: [UIImage] = [
        UIImage(named: "JFYImage1")!,UIImage(named: "JFYImage2")!,UIImage(named: "JFYImage3")!]
//
     var promoDescription:[String] = ["select sale- & clearance-priced appaarel, shoes, accessories,jewelry & home","select sale- & clearance-priced appaarel, shoes, accessories,jewelry & home","select sale- & clearance-priced appaarel, shoes, accessories,jewelry & home","select sale- & clearance-priced appaarel, shoes, accessories,jewelry & home","select sale- & clearance-priced appaarel, shoes, accessories,jewelry & home"]
    
    let JFYTitles:[String] = ["Recomended For You","Our Customers Love","Trending Now"]
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return JFYTitles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let JFYCell = tableView.dequeueReusableCell(withIdentifier: "JFYCell", for: indexPath) as! JustForViewTableViewCell
        JFYCell.JFYTitle.text = JFYTitles[indexPath.item]
        JFYCell.JFYImageview.image = JFYImages[indexPath.item]
        return JFYCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         if collectionView == self.adsColView {
            return  images.count
        } else if collectionView == self.promColView {
             return  promoArray.count
         } else {
            return recentViewedImages.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.adsColView {
            let offercell = collectionView.dequeueReusableCell(withReuseIdentifier: "offers", for: indexPath) as! offerCell
            offercell.offerImageView.image = images[indexPath.item]
    
            
             return offercell
            
        } else if collectionView == self.promColView {
            
            let promocell = collectionView.dequeueReusableCell(withReuseIdentifier: "promoCodes", for: indexPath) as! promoCell
            promocell.promoLabel.text = promoArray[indexPath.item]
            promocell.descriptionLabel.text = promoDescription[indexPath.item]
            
            return promocell
            
        } else {
            let recentcell = collectionView.dequeueReusableCell(withReuseIdentifier: "recentlyViewed", for: indexPath) as! recentlyViewedCell
            recentcell.recentlyViewedImageView.image =   recentViewedImages[indexPath.item]
            
            return recentcell
            
        }
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
    //     setupNavisgationItems()
         initializeCollectionView()
    }
    
    private func setupNavisgationItems(){
    
        let titleText = "JCPenny"
        navigationItem.title = titleText
}
    
    private func initializeCollectionView(){
        
        adsColView.delegate = self
        adsColView.dataSource = self
        
        promColView.dataSource = self
        promColView.delegate = self
        
        recentlyViewedColView.dataSource = self
        recentlyViewedColView.delegate = self
    }
    

}
